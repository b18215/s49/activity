console.log("Hi")


//https://jsonplaceholder.typicode.com/
// Placeholder 



//syntax:
	// fetch)'url', options)
		// url - this is the url which the request is to be made.
		// options - an array of properties, optional parameter.



//Show Posts
fetch('https://jsonplaceholder.typicode.com/posts').then( response => response.json()).then(data => showPosts (data))

const showPosts = (posts) => {

	let postEntries = "";

	posts.forEach(post => {
		console.log(post)

		postEntries += `
		<div id="post-${post.id}">
			<h3 id='post-title-${post.id}'>${post.title}</h3>
			<p id='post-body-${post.id}'>${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}


// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) =>{
		//prevent the page from loading
		e.preventDefault();

		fetch('https://jsonplaceholder.typicode.com/posts', {

			method: 'POST',
			body: JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 1
			}),
			headers: {
				'Content-Type' : 'application/json'
			}
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data)
			alert('Sucessfully added');


			//Query selector = null, resets the state of our input into blank after submitting a new post.
			document.querySelector('#txt-title').value = null;
			document.querySelector('#txt-body').value = null;
		})
	})


//Edit Post

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled')
}


//Update Post

document.querySelector("#form-edit-post").addEventListener('submit', (e) =>{
	e.preventDefault()
	fetch('https://jsonplaceholder.typicode.com/posts/1', {

			method: 'PUT',
			body: JSON.stringify({
				id: document.querySelector('#txt-edit-id').value,
				title: document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId: 1
			}),
			headers: {
				'Content-Type' : 'application/json'
			}
		})
		.then(response => response.json())
		.then(data =>{
			console.log(data)
			alert('Post Sucessfully updated')
				
				//Query selector = null, resets the state of our input into blank after submitting a new post.
			document.querySelector('#txt-edit-title').value = null;
			document.querySelector('#txt-edit-body').value = null;	
			
			//to disable the button
			document.querySelector("#btn-submit-update").setAttribute('disabled', true)

		})
	})


// Delete post data

function deletePost(id){
        const url = `https://jsonplaceholder.typicode.com/posts/id`;
        const deleteObject = { method: 'DELETE' };
        fetch(url, deleteObject)
        .then(document.querySelector(`#post-${id}`).remove() )

        console.log(`deleted item ${id}`)
        alert('Sucessfully deleted.')
      }
